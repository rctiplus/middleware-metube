FROM golang:1.13
COPY . .
RUN go build -o metube-middleware

FROM ubuntu:20:04
COPY --from=0 /metube-middleware .
CMD ["./metube-middleware"]
