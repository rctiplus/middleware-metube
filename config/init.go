package config

import (
	"log"
	"os"
	"strings"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var (
	App *Application
)

type (
	Application struct {
		Name    string            `json:"name"`
		Port    string            `json:"port"`
		Version string            `json:"version"`
		ENV     string            `json:"env"`
		Config  viper.Viper       `json:"prog_config"`
		Cookies map[string]string `json:"cookies"`
	}
)

// Initiate news instances
func init() {
	var err error
	App = &Application{}
	App.Name = "metube-middleware"
	App.Version = os.Getenv("APPVER")
	if err = App.LoadConfigs(); err != nil {
		log.Printf("Load config error : %v", err)
	}

	// apply custom validator
	App.Port = App.Config.GetString("port")
	App.ENV = App.Config.GetString("env")
}

func (x *Application) Close() (err error) {

	return nil
}

// Loads general configs
func (x *Application) LoadConfigs() error {
	var conf *viper.Viper

	conf = viper.New()
	conf.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	conf.AutomaticEnv()
	conf.SetConfigName("config")
	conf.AddConfigPath(".")
	conf.SetConfigType("yaml")
	if err := conf.ReadInConfig(); err != nil {
		return err
	}
	conf.WatchConfig()
	conf.OnConfigChange(func(e fsnotify.Event) {
		log.Println("App Config file changed %s:", e.Name)
		x.LoadConfigs()
	})
	x.Config = viper.Viper(*conf)
	return nil
}
