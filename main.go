package main

import (
	"fmt"
	"net/http"
	"os"

	"metube-middleware/config"
	_ "metube-middleware/docs" // docs is generated by Swag CLI, we have to import it.
	"metube-middleware/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	"github.com/tus/tusd/pkg/filestore"
	tusd "github.com/tus/tusd/pkg/handler"
)

// @title API Documentation for Middleware VOTE
// @version 0.0.1
// @description Dokumentasi API untuk website rctiplus.com

// @contact.name UGC VOTE
// @contact.url https://rctiplus.com
// @contact.email support@rctiplus.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

func main() {
	defer config.App.Close()

	e := echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time:${time_rfc3339_nano} ,method=${method}, uri=${uri}, status=${status} , remote_ip=${remote_ip} , error=${error} , host=${host} \n",
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))

	store := filestore.FileStore{
		Path: "./uploads",
	}

	// A storage backend for tusd may consist of multiple different parts which
	// handle upload creation, locking, termination and so on. The composer is a
	// place where all those separated pieces are joined together. In this example
	// we only use the file store but you may plug in multiple.
	composer := tusd.NewStoreComposer()
	store.UseIn(composer)

	// Create a new HTTP handler for the tusd server by providing a configuration.
	// The StoreComposer property must be set to allow the handler to function.
	handler, err := tusd.NewHandler(tusd.Config{
		BasePath:              "/files/",
		StoreComposer:         composer,
		NotifyCompleteUploads: true,
	})
	if err != nil {
		panic(fmt.Errorf("Unable to create handler: %s", err))
	}

	// Start another goroutine for receiving events from the handler whenever
	// an upload is completed. The event will contains details about the upload
	// itself and the relevant HTTP request.
	go func() {
		for {
			event := <-handler.CompleteUploads
			fmt.Printf("Upload %s finished\n", event.Upload.ID)
		}
	}()

	e.GET("/docs/*", echoSwagger.WrapHandler)
	http.Handle("/files/", http.StripPrefix("/files/", handler))
	api := e.Group("/api")
	{
		api.GET("/serviceinfo", handlers.ServiceInfo)

		// Just for example purpose
		accounts := api.Group("/accounts")
		accounts.POST("/login", handlers.Login)
	}

	e.Logger.Fatal(e.Start(":" + config.App.Port))
	os.Exit(0)
}
