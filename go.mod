module metube-middleware

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9
	github.com/labstack/echo/v4 v4.1.16
	github.com/spf13/viper v1.7.0
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.7
	github.com/thedevsaddam/govalidator v1.9.10
	github.com/tus/tusd v1.3.0
	github.com/valyala/fasttemplate v1.1.1 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
)
