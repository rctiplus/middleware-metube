package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

// Info main type
type Info struct {
	Time string `json:"time"`
	// DB   bool   `json:"database"`
}

var (
	err  error
	info Info
)

// Service Info for the system
// @Summary Check System if ready
// @Description Service Info for the system
// @Tags service
// @ID service-info
// @Produce text/json
// @Success 200 {string} string	"ok"
// @Router /api/serviceinfo [get]
func ServiceInfo(c echo.Context) error {
	defer c.Request().Body.Close()

	info.Time = fmt.Sprintf("%v", time.Now().Format("2006-01-02T15:04:05"))

	return c.JSON(http.StatusOK, info)
}
